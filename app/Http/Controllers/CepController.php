<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CepController extends Controller
{
    public function getAddress(Request $request){
        //Pega o cep que vem da request (front end)
        $cep = $request['cep'];
        // Url da api
        $url = "viacep.com.br/ws/{$cep}/json/";
        // Biblioteca para fazer requisições para api externas
        $curl = new \anlutro\cURL\cURL;
        // Função que faz a chamada get para a api e retorna um resultado
        $request = $curl->newRequest('get', $url)->send();
        // Converte o resultado json em um array 
        $viaCep = json_decode($request->body);
        
        // Retorna o resultado para o front

        return view('address')->with(compact('viaCep'));
    }
}
