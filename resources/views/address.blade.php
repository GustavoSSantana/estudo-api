<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.88.1">
    <title>Busca de Cep</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.6/examples/checkout/">



    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css"
        integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">



    <!-- Favicons -->
    <link rel="apple-touch-icon" href="/docs/4.6/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="/docs/4.6/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="/docs/4.6/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="/docs/4.6/assets/img/favicons/manifest.json">
    <link rel="mask-icon" href="/docs/4.6/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
    <link rel="icon" href="/docs/4.6/assets/img/favicons/favicon.ico">
    <meta name="msapplication-config" content="/docs/4.6/assets/img/favicons/browserconfig.xml">
    <meta name="theme-color" content="#563d7c">


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

    </style>


    <!-- Custom styles for this template -->
    <link href="form-validation.css" rel="stylesheet">
</head>

<body class="bg-light">

    <div class="container">
        <div class="py-5 text-center">
            {{-- <img class="d-block mx-auto mb-4" src="/docs/4.6/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72"> --}}
            <h2>Busca de Cep</h2>
            <p class="lead">Busca de cep pela Api do ViaCep <a>https://viacep.com.br/</a></p>
        </div>

        <form action="/getaddress" method="POST">
        {{ csrf_field() }}
        <div class="row">
                <div class="col-md-4">
                    <label>Informe um Cep</label>
                    <div class="input-group">
                        <input type="text" name="cep" class="form-control" placeholder="CEP">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-secondary">Buscar Endereço</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        
        <hr class="mb-4">

        <div class="row">
            <div class="col-md-6 mt-2">
                <label>Rua</label>
                <input class="form-control" value="{{ $viaCep->logradouro ?? '' }}" disabled>
            </div>
            <div class="col-md-3 mt-2">
                <label>Bairro</label>
                <input class="form-control" value="{{ $viaCep->bairro ?? '' }}" disabled>
            </div>
            <div class="col-md-3 mt-2">
                <label>Cidade</label>
                <input class="form-control" value="{{ $viaCep->localidade ?? '' }}" disabled>
            </div>
            <div class="col-md-2 mt-2">
                <label>Estado</label>
                <input class="form-control" value="{{ $viaCep->uf ?? '' }}" disabled>
            </div>
            <div class="col-md-2 mt-2">
                <label>DDD</label>
                <input class="form-control" value="{{ $viaCep->ddd ?? '' }}" disabled>
            </div>
        </div>

        <footer class="my-5 pt-5 text-muted text-center text-small">
            <p class="mb-1">&copy; 2021-2021 Álvaro Company</p>
        </footer>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script>
        window.jQuery || document.write('<script src="/docs/4.6/assets/js/vendor/jquery.slim.min.js"><\/script>')
    </script>


    <script src="form-validation.js"></script>
</body>

</html>
